"""CampaignMonitor target sink class, which handles writing streams."""


from singer_sdk.sinks import BatchSink
import requests
import phonenumbers


class CampaignMonitorSink(BatchSink):
    """CampaignMonitor target sink class."""

    max_size = 10000  # Max records to write in one batch
    subscribers = []
    unsubscribers = []
    list_id = None

    @property
    def base_url(self):
        return "https://api.createsend.com/api/v3.3"

    def get_headers(self):
        headers = {}

    def _request(self, stream, payload=None, method="GET"):
        if payload is None:
            res = requests.request(
                method,
                f"{self.base_url}/{stream}",
                auth=(self.config.get("api_key"), "x"),
            )
        if payload:
            res = requests.request(
                method,
                f"{self.base_url}/{stream}",
                auth=(self.config.get("api_key"), "x"),
                json=payload,
            )
        try:
            res = res.json()
        except:
            res = res.text
        return res

    def get_list_id(self):
        if self.list_id:
            return self.list_id
        # first get clients
        clients = self._request("clients.json")
        if len(clients) > 0:
            # Lets assume there is only one client. If required, get using config.
            clients = clients[0]["ClientID"]
            lists = self._request(f"clients/{clients}/lists.json")
            for list in lists:
                if list.get("Name") == self.config.get("list_name"):
                    self.list_id = list.get("ListID")

        # if no list is found raise an exception and stop the job
        if self.list_id is None:
            raise Exception("No list with provided name found.")
        return self.list_id

    def process_record(self, record: dict, context: dict) -> None:
        #TODO figure out what is ConsentToTrack's impact
        if self.stream_name.lower() in ["contacts", "contact", "customer", "customers"]:
            subscriber = {
                "EmailAddress": record.get("email"),
                "Name": record.get("name"),
                "ConsentToTrack": "Yes",
            }
            if record.get("first_name") and record.get("last_name"):
                subscriber["Name"] = f"{record.get('first_name')} {record.get('last_name')}"
            #Validate phone number
            try:
                phone_number = record.get("phone") 
                if phone_number:
                    phone_number = phonenumbers.parse(phone_number)
                    if phonenumbers.is_valid_number(phone_number):             
                        subscriber["MobileNumber"] = record.get("phone")
            except:
                #Fail the job? Letting it pass for now            
                pass
            if record.get('subscribe_status')=='subscribed':                
                self.subscribers.append(subscriber)
            elif record.get('subscribe_status')=='unsubscribed':
                self.unsubscribers.append({"EmailAddress": record.get("email")})

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        if self.stream_name.lower() in ["contacts", "contact", "customer", "customers"]:
            if len(self.subscribers) > 0:
                list_id = self.get_list_id()
                res = self._request(
                    f"subscribers/{list_id}/import.json",
                    payload={"Subscribers": self.subscribers},
                    method="POST",
                )
                print(res)
            if len(self.unsubscribers)>0:
                list_id = self.get_list_id()
                for unsubscribe in self.unsubscribers:
                    res = self._request(
                        f"subscribers/{list_id}/unsubscribe.json",
                        payload=unsubscribe,
                        method="POST",
                    )
                    print(res)

            else:
                print("No subscribers loaded")
