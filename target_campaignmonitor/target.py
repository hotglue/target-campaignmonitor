"""CampaignMonitor target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_campaignmonitor.sinks import (
    CampaignMonitorSink,
)


class TargetCampaignMonitor(Target):
    """Sample target for CampaignMonitor."""

    name = "target-campaignmonitor"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
        ),
        th.Property("list_name", th.StringType, required=True),
    ).to_dict()
    default_sink_class = CampaignMonitorSink


if __name__ == "__main__":
    TargetCampaignMonitor.cli()
